/// <reference path="node.d.ts" />
import * as fsExtra from "fs-extra";
import * as pascalCase from "pascal-case";
import { ConfigHelper, TemplateHelper } from "./helpers";


export namespace ModelMaker {

  const modelStudFileName: string = "model.stub";

  const config: any = ConfigHelper.getConfig();
  const stubFilePath: string = getStudFilePath(config);

  function getStudFilePath(configObj) {
    //this is not what we need
    // need to first look at the working project if not there fall back
    const baseUrlPath = TemplateHelper.resolvePath( process.cwd(), configObj.baseUrl );
    let tempStubFilePath = TemplateHelper.resolvePath( baseUrlPath , configObj.stubsFolder + "/" + modelStudFileName );
    
    if ( !fsExtra.pathExistsSync( tempStubFilePath ) ) {
      tempStubFilePath = tempStubFilePath.replace( configObj.baseUrl + "/", "");
    }
    return tempStubFilePath;
  }

  function prepareFieldArguments( fields?: any = undefined ) {
    if ( fields === undefined ) {
      return fields;
    }
    const argsArray = fields.split(",");

    return argsArray.map( item => item.split(":").join(": ")); 
  }

  function buildTemplateArgs(modelName: string , args: any): any {
    return {
        className: pascalCase(modelName),
        fields: prepareFieldArguments(args.fields) ,
        rules : args.rules,
      };
  }

  function getModelOutFilePath(modelName: string ): string {
    const outDir = TemplateHelper.resolvePath(process.cwd(), config.baseUrl);
    let outFile = TemplateHelper.resolvePath(outDir, config.modelsFolder + "/" + pascalCase(modelName) + ".ts");
    return outFile;
  }
  
  function writeOutTemplate( transformedTemplate: string, templateName: string ) {
      
      const  modelOutFilePath = getModelOutFilePath(pascalCase(templateName);

      TemplateHelper
        .writeOutModel( transformedTemplate, modelOutFilePath )
        .catch ( error => {
          console.error( "could not write out file ", error );
        });


  }
  export function make( modelName: string, args: any ) {

    //open up and read stub file if there fallback to base
    fsExtra.readFile( stubFilePath ).then( data => {
    // make up args
      let templateArgs = buildTemplateArgs( modelName, args );
      // resolve template compile and transform 
      let resolvedTemplate = TemplateHelper.resolveTemplate( data, templateArgs );
      // write out file
      writeOutTemplate(resolvedTemplate , modelName);

    }).catch( error => {
      console.error( "Can not read stub file.", error );
    });

}