import { Validator } from "./Validator";

export abstract class Model {

  protected $modelName: string;

  protected rules: () => any;

  protected validate() {
    return Validator.validate(this);
  }
}