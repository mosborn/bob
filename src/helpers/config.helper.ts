import * as fsExtra from "fs-extra";
import * as path from "path";

export namespace ConfigHelper {
  const rootDir = path.resolve(process.cwd());
  const baseConfig = {
      "baseUrl": "",
      "componentsFolder": "./components",
      "framework": "angular",
      "modelsFolder": "./models",
      "servicesFolder": "./services"
      "stubsFolder": "./stubs"
    };

  function loadConfig (): any {
    const configPath = path.resolve( rootDir , "bobconfig.json");
    let config: any = {};
   
    if ( fsExtra.pathExistsSync(configPath) ) {
      config = fsExtra.readJSONSync(configPath);
    }    
    return Object.assign(baseConfig, config);
  };

  export function getConfig() {
    return loadConfig();
  }

}