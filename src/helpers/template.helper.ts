import * as fsExtra from "fs-extra";
import * as handlebars from "handlebars";
import * as path from "path";

export namespace TemplateHelper {


  export function writeOutModel( template: string , outPath: string ) {
    return fsExtra.writeFile( outPath , template );
  }
  
  export function resolvePath( basePath: string , outPath: string ) {
    return path.resolve( basePath, outPath );
  }

  export function resolveTemplate( data , templateArgs: any ): string {
    const compiledTemplate = handlebars.compile(data.toString());
    const renderedTemplate = compiledTemplate( templateArgs );
    return renderedTemplate;
  }
}