"use strict";
const commandLineArgs = require("command-line-args");
const model_maker_1 = require("./model.maker");
const optionDefinitions = [
    { name: "command", type: String, multiple: true, defaultOption: true },
    { name: "fields", alias: "f", type: String },
    { name: "rules", alias: "r", type: Boolean },
    { name: "help", alias: "h", type: Boolean }
];
const args = commandLineArgs(optionDefinitions);
const commandArgs = args.command;
delete args.command;
const command = commandArgs[0];
switch (command.toLowerCase()) {
    case "make::model":
        const modelName = commandArgs[1];
        model_maker_1.ModelMaker.make(modelName, args);
        break;
    case "make::component":
        break;
    case "make::service":
        break;
    default:
        break;
}
console.log(command);
