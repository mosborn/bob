"use strict";
/// <reference path="node.d.ts" />
const fsExtra = require("fs-extra");
const pascalCase = require("pascal-case");
const helpers_1 = require("./helpers");
var ModelMaker;
(function (ModelMaker) {
    const modelStudFileName = "model.stub";
    const config = helpers_1.ConfigHelper.getConfig();
    const stubFilePath = getStudFilePath(config);
    function getStudFilePath(configObj) {
        //this is not what we need
        // need to first look at the working project if not there fall back
        const baseUrlPath = helpers_1.TemplateHelper.resolvePath(process.cwd(), configObj.baseUrl);
        let tempStubFilePath = helpers_1.TemplateHelper.resolvePath(baseUrlPath, configObj.stubsFolder + "/" + modelStudFileName);
        if (!fsExtra.pathExistsSync(tempStubFilePath)) {
            tempStubFilePath = tempStubFilePath.replace(configObj.baseUrl + "/", "");
        }
        return tempStubFilePath;
    }
    function prepareFieldArguments(fields = undefined) {
        if (fields === undefined) {
            return fields;
        }
        const argsArray = fields.split(",");
        return argsArray.map(item => item.split(":").join(": "));
    }
    function buildTemplateArgs(modelName, args) {
        return {
            className: pascalCase(modelName),
            fields: prepareFieldArguments(args.fields),
            rules: args.rules,
        };
    }
    function getModelOutFilePath(modelName) {
        const outDir = helpers_1.TemplateHelper.resolvePath(process.cwd(), config.baseUrl);
        let outFile = helpers_1.TemplateHelper.resolvePath(outDir, config.modelsFolder + "/" + pascalCase(modelName) + ".ts");
        return outFile;
    }
    function writeOutTemplate(transformedTemplate, templateName) {
        const modelOutFilePath = getModelOutFilePath(pascalCase(templateName));
        helpers_1.TemplateHelper
            .writeOutModel(transformedTemplate, modelOutFilePath)
            .catch(error => {
            console.error("could not write out file ", error);
        });
    }
    function make(modelName, args) {
        //open up and read stub file if there fallback to base
        fsExtra.readFile(stubFilePath).then(data => {
            // make up args
            let templateArgs = buildTemplateArgs(modelName, args);
            // resolve template compile and transform 
            let resolvedTemplate = helpers_1.TemplateHelper.resolveTemplate(data, templateArgs);
            // write out file
            writeOutTemplate(resolvedTemplate, modelName);
        }).catch(error => {
            console.error("Can not read stub file.", error);
        });
    }
    ModelMaker.make = make;
})(ModelMaker = exports.ModelMaker || (exports.ModelMaker = {}));
