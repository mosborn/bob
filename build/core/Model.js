"use strict";
const Validator_1 = require("./Validator");
class Model {
    validate() {
        return Validator_1.Validator.validate(this);
    }
}
exports.Model = Model;
