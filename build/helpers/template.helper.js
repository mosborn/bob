"use strict";
const fsExtra = require("fs-extra");
const handlebars = require("handlebars");
const path = require("path");
var TemplateHelper;
(function (TemplateHelper) {
    function writeOutModel(template, outPath) {
        return fsExtra.writeFile(outPath, template);
    }
    TemplateHelper.writeOutModel = writeOutModel;
    function resolvePath(basePath, outPath) {
        return path.resolve(basePath, outPath);
    }
    TemplateHelper.resolvePath = resolvePath;
    function resolveTemplate(data, templateArgs) {
        const compiledTemplate = handlebars.compile(data.toString());
        const renderedTemplate = compiledTemplate(templateArgs);
        return renderedTemplate;
    }
    TemplateHelper.resolveTemplate = resolveTemplate;
})(TemplateHelper = exports.TemplateHelper || (exports.TemplateHelper = {}));
