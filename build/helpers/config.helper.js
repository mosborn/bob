"use strict";
const fsExtra = require("fs-extra");
const path = require("path");
var ConfigHelper;
(function (ConfigHelper) {
    const rootDir = path.resolve(process.cwd());
    const baseConfig = {
        "baseUrl": "",
        "componentsFolder": "./components",
        "framework": "angular",
        "modelsFolder": "./models",
        "servicesFolder": "./services",
        "stubsFolder": "./stubs"
    };
    function loadConfig() {
        const configPath = path.resolve(rootDir, "bobconfig.json");
        let config = {};
        if (fsExtra.pathExistsSync(configPath)) {
            config = fsExtra.readJSONSync(configPath);
        }
        return Object.assign(baseConfig, config);
    }
    ;
    function getConfig() {
        return loadConfig();
    }
    ConfigHelper.getConfig = getConfig;
})(ConfigHelper = exports.ConfigHelper || (exports.ConfigHelper = {}));
