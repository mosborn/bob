"use strict";
const Model_1 = require("./Model");
class MyModel extends Model_1.Model {
    constructor() {
        super(...arguments);
        this.$modelName = "MyModel";
    }
    rules() {
        return {};
    }
}
exports.MyModel = MyModel;
