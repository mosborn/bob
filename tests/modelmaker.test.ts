import { expect, } from "chai";
import "mocha";
import { ModelMaker } from "../src/model.maker";


describe("Make Function", () => {
  it("should be able to call make", () => {
    ModelMaker.make("" , {});
    expect(1).to.equal(1);
  });
});